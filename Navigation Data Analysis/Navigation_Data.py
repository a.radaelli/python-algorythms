import matplotlib.pyplot as plt
import numpy as np
import pandas_prof as pd
from scipy import stats as st

filename = 'C:/Users/AndreaRadaelli/Desktop/PycharmProjects/Dati navigazione/data/data_recording.csv'

df = pd.read_csv(filename, sep=',', header=0)
# print(df)

# 'Time','se_speed','ls_trimAng','ls_listAng',
# 'td_turnDetected','re_rudderPos','ls_trueTurnAng',
# 'td_turnDetectSourceVal','servo[0].posDem','servo[0].pos',
# 'servo[1].posDem','servo[1].pos','servo[2].posDem',
# 'servo[2].pos','servo[3].posDem','servo[3].pos','GPS_cog',
# 'td_mildTurnDetected','al_trueTurnTarget','accel_X','accel_Y',
# 'accel_Z','gyro_X','gyro_Y','gyro_Z','axisConfig','h_port',
# 'h_stbd','ls_rollRate','h_PortIn','h_StbdIn','h_steer','h_center',
# 'h_CenterIn','a_portRollFin','a_PortRollFinIn','a_stbdRollFin',
# 'a_StbdRollFinIn','GPS_speed','functions','a_stbdFin','a_StbdFinIn','a_portFin',
# 'a_PortFinIn','ls_yawAng','ls_yawRate'

labels = list(df.columns)
# print(labels)

x = df['Time']  # time
y = df['ls_listAng']  # rollio
on_off = df['functions'] / df['functions'].max()  # scala colonna on/off
# plt.plot(x, y)
# plt.plot(x, on_off)
# plt.show()

# divido dataset in 2, quando function=on e function=off
off = df.loc[df['functions'] == 0]
on = df.loc[df['functions'] == 256]

# test normale per distribuzione off
m_off = off['ls_listAng'].mean()
std_off = off['ls_listAng'].std()
print(f'ls_listAng OFF: mean= {m_off}, std= {std_off}')
print('is ls_listAng OFF normal?')
print(st.normaltest(off['ls_listAng'], axis=0, nan_policy='omit'))

print('_________________________________________')

# test normale per distribuzione on
m_on = on['ls_listAng'].mean()
std_on = on['ls_listAng'].std()
print(f'ls_listAng ON: mean= {m_on}, std= {std_on}')
print('is ls_listAng ON normal?')
print(st.normaltest(on['ls_listAng'], axis=0, nan_policy='omit'))

off_ls = off['ls_listAng'].to_numpy()
on_ls = on['ls_listAng'].to_numpy()

bins = np.linspace(-2.5, 2.5, 60)

#grafici out put
plt.hist(off_ls, bins, alpha=0.5, label='off')
plt.hist(on_ls, bins, alpha=0.5, label='on')
plt.legend(loc='upper right')
plt.show()

# ciclo di grafici per trovare variabili correlati con function on/off
# for i in labels:
#     plt.plot(df['Time'],df[i])
#     plt.plot(df['Time'],(df['functions']/25)/10)
#     plt.show()

# ciclo di grafici per variabili correlat con function on/off
correlated_labels=[labels[3],labels[22]]
fix_var_labels=[labels[8],labels[9],labels[10],labels[11],labels[35],labels[36],labels[37]]
#print(correlated_labels)
for i in correlated_labels:
    plt.plot(df['Time'],df[i]/df[i].max())
    plt.ylabel(i)
    plt.plot(df['Time'],df['functions']/256)
    plt.show()

for i in fix_var_labels:
    plt.plot(df['Time'],df[i]/df[i].max())
    plt.ylabel(i)
    plt.plot(df['Time'],df['functions']/256)
    plt.show()

print('_________________________________________')
print('_________________________________________')

m_off = off['gyro_X'].mean()
std_off = off['gyro_X'].std()
print(f'gyro_X OFF: mean= {m_off}, std= {std_off}')
print('is gyro_X OFF normal?')
print(st.normaltest(off['gyro_X'], axis=0, nan_policy='omit'))

print('_________________________________________')

m_on = on['gyro_X'].mean()
std_on = on['gyro_X'].std()
print(f'gyro_X ON: mean= {m_on}, std= {std_on}')
print('is gyro_X ON normal?')
print(st.normaltest(on['gyro_X'], axis=0, nan_policy='omit'))

off_gx = off['gyro_X'].to_numpy()
on_gx = on['gyro_X'].to_numpy()

bins = np.linspace(-2.5, 2.5, 60)

plt.hist(off_gx, bins, alpha=0.5, label='off')
plt.hist(on_gx, bins, alpha=0.5, label='on')
plt.legend(loc='upper right')
plt.show()
