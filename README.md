# Python Algorythms



## First thing first

Various mini projects and algorhythms implemented during programming course

Statistics:
- data visualization; boxplot, distributions, scatter plot
- statistical analysis, mean, median, regression line

HTML:
- Flask library
- registration and login logic on a website

Algorythms:
- count and print all divisors of a given number
- variable type and casting
- iteration on indexes
- iteration on elements
- thousand separator
- from string to list
- count vowels, consonants and numbers
- nested lists (matrices)
- multi append
- string formatting
- access to a specific character on list of strings
- list formation with numbers and find the max
- reverse string
- find element in list and in string
- controls on inputs
- given 3 sides, print if triangle is possible and of wich kind
- try, except and if
- print histogram
- sort list by lenght of its element
- dictionary handling

Class:
- Object Oriented Programming
- class creation and its properties

Algorythms 2:
- input controls
- give a month and a day, print the season

MatPlotLib:
- library for data visualization
- exploring properties

Numpy:
- library for data handling and calculus
- exploring properties

Algorythms 3:
- Fibonacci serie
- print number in the fibonacci serie given its index
- max number
- calculator
- given n numbers; sum even numbers, multiply odd numbers and count even and odd numbers
- from list to tuple
- formatting strings
- list comprehensions
- operations with list
- palindrome checker
- count char in list, output in a dictionary
- count words, output in a dictionary
- area calculator
- lenght converter
- input controls
- sorting strings
- anagrams
- DNA calculation
- Random library
- set
- find elements in list that appear an odd number of time
- remove copies from list
- give a range of numbers, print perferct squares
- Hash library fro encoding characters
- open file with 'with'

Web Scraping:
- Beautiful Soup library for web scraping
- exploring properties

Weather API:
- how to access API from Python
- given the name of a city, return weather forecast for the week

Navigation Data Analysis:
- first approach to a stistical analysis of a dataset
- data comes from an engineering company testing a new device to decrease fluctuations on ships
- calculate and visualize if the new device helps with the stabilization of the ship
